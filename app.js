var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const sequelize = require('./helpers/database');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var hbs = require('express-handlebars');
var Handlebars = require('handlebars');
var app = express();
const Organiser = require('./models/organisers');
const Faqdata = require('./models/faqdata');
const Result = require('./models/result');
const Reg = require('./models/registeredmem');
const Paytm = require('./models/paytm');
const College = require('./models/college');
const registerRouter = require('./routes/register');
const resultsRouter = require('./routes/results');

const session = require('express-session');
var SequelizeStore = require('connect-session-sequelize')(session.Store);
const cors = require('cors');
const flash = require('connect-flash');
// const csrf = require('csurf');
// view engine setup
app.use(express.static(path.join(__dirname,'public')));
app.engine('hbs', hbs({layoutsDir: 'views/layouts/',partialsDir: __dirname +'/views/partials/', defaultLayout: 'main-layout', extname: 'hbs'}));
app.set('view engine', 'hbs');
app.set('views', 'views');

var myStore = new SequelizeStore({
  db: sequelize
})


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
  name: 'SSID',
  secret: 'proxyspotting xml',
  unset: 'destroy',
  saveUninitialised: false,
  store: myStore,
  //resave: false, // we support the touch method so per the express-session docs this should be set to false
  //proxy: true, // if you do SSL outside of node.
  checkExpirationInterval: 15 * 60 * 1000, // The interval at which to cleanup expired sessions in milliseconds.
  expiration: 24 * 60 * 60 * 1000  // The maximum age (in milliseconds) of a valid session.
}))


app.use(flash());
app.use(cors(corsOptions));

var corsOptions = {
  origin: 'https://www.manipalmarathon.in, http://localhost:3000',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(registerRouter);

// const csrfProtection = csrf({ cookie: true });



// app.use(csrfProtection);

// app.use((req,res,next) => {
//   var token = req.csrfToken();
//   res.cookie('XSRF-TOKEN', token);
//   res.locals.csrfToken = token;
//   // res.locals.csrfToken = req.csrfToken();
//   next();
// })


app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/results', resultsRouter);


sequelize.sync().then().catch(err => console.log(err));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//Custom handlebar helper
Handlebars.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  else if(String(v1)===String(v2)){
    return options.fn(this);
  }
  return options.inverse(this);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
