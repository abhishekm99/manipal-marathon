var express = require('express');
const { check, validationResult } = require('express-validator');
// const csrf = require('csurf');
var router = express.Router();
const registerController = require('../controllers/register');
const captcha = require('../helpers/recaptchaRedirect');


/* GET home page. */
router.get('/register', registerController.getRegisterPage);
router.post('/register',[
    check('email').isEmail().withMessage('Email must be a proper email'),
    check('name').isLength({ min: 5 }).withMessage('Name must be minimum 5 characters'),
    check('emgname').isLength({ min: 5 }).withMessage('Emergency Name must be minimum 5 characters'),
    check('age').isNumeric().withMessage('Age must be a number'),
    check('contact').isLength({min:10,max:10}).withMessage('Please enter a proper mobile number'),
    check('emgcontact').isLength({min:10,max:10}).withMessage('Please enter a proper emergency mobile number'),
  ],captcha,
   registerController.postRegister);
router.get('/payment', registerController.makePayments);
router.get('/paytmCallback', registerController.getPaytmCallback);
router.post('/paytmCallback', registerController.postPaytmCallback);
router.post('/getTxnStatus', registerController.getTransactionStatus);
router.post('/updatePaymentStatus', registerController.updatePaymentStatus);
router.get('/tesseract', registerController.getTesserect);
module.exports = router;
