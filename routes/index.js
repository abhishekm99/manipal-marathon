var express = require('express');
var router = express.Router();
const aboutController = require('../controllers/about');
const organiserController = require('../controllers/organisers');
const Organiser = require('../models/organisers');
const Faqdata = require('../models/faqdata.js');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { pageTitle: 'Manipal Marathon', notMain: false, });
});

router.get('/race-info', function(req,res,next) {
  Faqdata.findAll({order: [
    ['id', 'ASC'],
  ]}).then(result => {
      res.render('race-info', {
          pageTitle: 'Race Info',
          faq: result,
          notMain: true,
      })
  })
})

router.get('/race-info/race-types',function(req,res,next){
  res.render('race-types', { pageTitle: 'Manipal Marathon', notMain: true, });
})

router.get('/pastraces', function(req,res,next) {
  res.render('prev', {
    pageTitle: 'Previous Marathons',
    notMain: true,
  })
})

router.get('/training', function(req,res,next) {
  res.render('training', {
    pageTitle: 'Training Tips',
    notMain: true
  })
})

router.get('/organiser', organiserController.getOrganisers);

router.get('/about', aboutController.getAboutPage);

router.get('/prizes', aboutController.getPrizes);
router.get('/prizes/5k', aboutController.getPrizes5k);
router.get('/prizes/10k', aboutController.getPrizes10k);
router.get('/prizes/21k', aboutController.getPrizes21k);
router.get('/prizes/42k', aboutController.getPrizes42k);
router.get('/prizes/mahestud', aboutController.getPrizesmahestud);
router.get('/prizes/mahefac', aboutController.getPrizesmahefac);
router.get('/prizes/certificates', aboutController.getCertificates);
module.exports = router;
