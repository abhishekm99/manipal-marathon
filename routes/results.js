var express = require('express');
var router = express.Router();

var resultsController = require('../controllers/result');

/* GET users listing. */
router.get('/', resultsController.getMain);

router.get('/:rtype/:cat/:gender', resultsController.getFinal);

module.exports = router;
