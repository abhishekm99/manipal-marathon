
$(document).ready(function(){
    var num = $(".route-card");
    var previous = $(".lft");
    var next = $(".rght");

    $('.slide-route').click(function(e) {
        e.preventDefault();
        var firstMsg = $(this);

        // Where the page is currently:
        var curOffset = firstMsg.offset().top - $(document).scrollTop();
        $(document).scrollTop(firstMsg.offset().top-curOffset);
    });

    //////////MODAL OPENS ON route-card CLICK/TAP
    $(".route-card").click(function(e) {
        //alert(num.index(this));
        //get index of route-card to open corresponding modal
        $(".modal").eq(num.index(this)).addClass("active");
        e.preventDefault();
    });

    //////////MODAL CLOSE ON X CLICK/TAP
    $(".modal-close").click(function(e) {
        $(".modal").removeClass("active");
        e.preventDefault();
    });

    //////////PREVIOUS ARROW 
    $(".lft").click(function(e) {
        // remove transition
        $(".modal").addClass('no-transition'); 
        //hides current modal
        $(".modal").eq(previous.index(this)).removeClass("active");
        //opens next modal
        $(".modal").eq(previous.index(this) - 1).addClass("active");
        // return to transition after 0.3s
        setTimeout(function(){
            $(".modal").removeClass('no-transition'); 
        },300); 
        e.preventDefault();
    });

    //////////NEXT ARROW
    $(".rght").click(function(e) {
        // remove transition
        $(".modal").addClass('no-transition'); 
        //hides current modal
        $(".modal").eq(next.index(this)).removeClass("active");
        //opens next modal
        $(".modal").eq(next.index(this) + 1).addClass("active");
        // return to transition after 0.3s
        setTimeout(function(){
            $(".modal").removeClass('no-transition'); 
        },300); 
        e.preventDefault();
    });

    //////////LAST NEXT ARROW
    $(".last-next").click(function(e) {
        // remove transition
        $(".modal").addClass('no-transition'); 
        //hides current modal
        $(".modal").eq(next.index(this)).removeClass("active");
        //opens first modal
        $(".modal").eq( 0 ).addClass("active");
        // return to transition after 0.3s
        setTimeout(function(){
            $(".modal").removeClass('no-transition'); 
        },300); 
        e.preventDefault();
    });

    
})
