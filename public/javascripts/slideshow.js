$(document).ready(function(){
    var options = {
        accessibility: true,
        prevNextButtons: true,
        pageDots: true,
        setGallerySize: false,
        arrowShape: {
          x0: 10,
          x1: 60,
          y1: 50,
          x2: 60,
          y2: 45,
          x3: 15
        }
      };
      
      var carousel = document.querySelector('[data-carousel]');
      var slides = document.getElementsByClassName('carousel-cell');
      var flkty = new Flickity(carousel, options);
      
      flkty.on('scroll', function () {
        flkty.slides.forEach(function (slide, i) {
          var image = slides[i];
          var x = (slide.target + flkty.x) * -1/3;
          image.style.backgroundPosition = x + 'px';
        });
      });
      
      var i;
      
      for(i=1;i<=10;i++){
          $(".flickity-slider").append(
            '\
            <div class="carousel-cell" style="background-image:url(../images/slides/${i}.JPG);">\
                  <div class="overlay"></div>\
                  <div class="inner">\
                  <h3 class="subtitle">Slide 1</h3>\
                  <h2 class="title">Flickity Parallax</h2>\
                  <a href="https://flickity.metafizzy.co/" target="_blank" class="btn">Tell me more</a>\
                  </div>\
            </div>\
            '
          );
      }
})