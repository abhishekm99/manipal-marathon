$(document).ready(function(){
    $(function(){
        $('body').height($(window).height() * $('section').length);
            $(window).on('scroll',function(){

                for(var j=0; j <= $('section').length;j++){
                    var n=j + 1;
                    $("section:nth-of-type(" + n + ")" ).css("z-index",$('section').length-j); 
                } 
                for (var i=0; i <=$('section').length-2; i++){
                    var n=i+1; 
                    if($(window).scrollTop()> $(window).height() * i) {
                        $("section:nth-of-type("+n+")").css({
                            transform: 'rotate(' + ($(window).scrollTop() - $(window).height() * i) / 15 + 'deg)',
                            opacity: 100 / ($(window).scrollTop() - $(window).height() * i) - 0.2
                        });
                    } else {
                        $("section:nth-of-type("+n+")").css({
                            transform: 'rotate(0deg)',
                            opacity: 1
                        });
                    }
                }
            });
        });

})