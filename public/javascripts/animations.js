$(document).ready(function(){
    var isInViewport = function (elem) {
        var distance = elem.getBoundingClientRect();
        return (
            distance.top >= -200 &&
            distance.left >= 0 &&
            distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            distance.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    };

    var flexcontainer = document.querySelector('.flex-container');
    var flexcontainer2 = document.querySelector('.flex-container-two');
    TweenMax.to('.training-logo',1,{opacity:1, y:0})
    TweenMax.to('#title-mm-train',1,{opacity:1, y:0})
    TweenMax.to('#sub-title',1.5,{opacity:1, x:0})
    TweenMax.to('.links-training',1.5,{opacity:1, x:0})
    const controller = new ScrollMagic.Controller();
        $('.card-week').each(function() {
            var title = $(this).find(".title");
            var option = $(this).find(".option")
            var tl = new TimelineMax();
            tl.from(title, .5, {opacity:0, scale:0});
            tl.from(option, .6, {opacity:0, scale:0});
            const scene = new ScrollMagic.Scene({
                triggerElement: this
            }).setTween(tl).addTo(controller); 
        });
        window.addEventListener('scroll', function (event) {
            if (isInViewport(flexcontainer)) {
                TweenMax.to('.img-div',1.0,{opacity:1, x:0})
                TweenMax.to('.title-two',.5,{opacity:1, scale:1})
                TweenMax.to('.info-two',1.5,{opacity:1, x:0})
            }
            if (isInViewport(flexcontainer2)) {
                TweenMax.to('.img-div-two',1.0,{opacity:1, x:0})
                TweenMax.to('.title',.5,{opacity:1, scale:1})
                TweenMax.to('.info',1.5,{opacity:1, x:0})
            }
        }, false);
}, false);


    /* if (isInViewport(this)) {
        alert(this);
        alert('in view');
        TweenMax.to(this,1.5,{opacity:1, y:0})
    } */

    /* $('#gen').click(function(){
        TweenMax.to('#general-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#tech').click(function(){
        TweenMax.to('#tech-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#sports').click(function(){
        TweenMax.to('#sports-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#cult').click(function(){
        TweenMax.to('#cult-container',1.5,{opacity:1, y:0, display:"flex"})
    })
    $('#support').click(function(){
        TweenMax.to('#support-container',1.5,{opacity:1, y:0, display:"flex"})
    }) */