// $(function() {
//   $('.intro').addClass('go');

//   $('.reload').click(function() {
//     $('.intro').removeClass('go').delay(200).queue(function(next) {
//       $('.intro').addClass('go');
//       next();
//     });

//   });
// })
$(document).ready(function(){

  //Landing animations 2.0

  var isInViewport = function (elem) {
    var distance = elem.getBoundingClientRect();
    return (
        distance.top >= -200 &&
        distance.left >= 0 &&
        distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        distance.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

var lefttitles = document.querySelector('.aboutdata');
var righttitles = document.querySelector('#righttitles');
TweenMax.to('.landingavatar',1.0,{opacity:1, transform:'scale(1)'})
TweenMax.to('.titlediv',1.0,{opacity:1, x: 0,onComplete: function(){
  TweenMax.to('.subtitlediv',1,{opacity: 1, x:0});
}})

// var pos = $('.aboutdata').scrollTop();
//       if (pos == 0) {
//           alert('top of the div');
//       }

var aboutdata = $('.aboutdata').offset().top;
var secondcontent = $('#righttitles').offset.top;

$(window).scroll(function() {
    if ( $(this).scrollTop() >= (aboutdata-200) ) {
      TweenMax.to('#lefttitles',1,{opacity: 1, x:0});
      TweenMax.to('#leftcontent',1,{opacity: 1, x:0});
    }
    if($(this).scrollTop() >= (aboutdata+100) ){
      TweenMax.to('#righttitles',1,{opacity: 1, x:0});
      TweenMax.to('#rightcontent',1,{opacity: 1, x:0});
    }
});
      if (isInViewport(lefttitles)) {
        TweenMax.to('#righttitles',1,{opacity: 1, x:0});
        TweenMax.to('#rightcontent',1,{opacity: 1, x:0});
      }
window.addEventListener('scroll', function (event) {
  console.log(isInViewport(lefttitles))
      
  // if ($('.aboutdata').scrollTop() == 0) {
  //   this.alert('hello');
  //   TweenMax.to('#lefttitles',1,{opacity: 1, x:0});
  //   TweenMax.to('#leftcontent',1,{opacity: 1, x:0});
  // }

  if (isInViewport(righttitles)) {
    TweenMax.to('#righttitles',1,{opacity: 1, x:0});
    TweenMax.to('#rightcontent',1,{opacity: 1, x:0});
  }

}, false);



  function typeEffect(element, speed) {
    var text = element.innerHTML;
    element.innerHTML = "";
    
    var i = 0;
    var timer = setInterval(function() {
      if (i < text.length) {
        element.append(text.charAt(i));
        i++;
      } else {
        clearInterval(timer);
      }
    }, speed);
  }
  
  
  // application
  
  var h1 = document.querySelector('#title-mm-train');
  if($('.container_svg').css('display')=='none'){
    var delay = 1000;
    var speed = 50;
  }
  else{
    var delay = 3900;
    var speed = 30;
  }
  
  
  // type affect to body
  setTimeout(function(){
    h1.style.display = "block";
    typeEffect(h1, speed);
  }, delay);
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

  for(i=1;i<=10;i++){
    $( ".c-slider-init" ).append(
      "\
      <div class='c-slide' style='background-image:url(../images/slides/"+i+".JPG)'>\
        <div class='c-slide-content'>\
          <div class='c-wrap c-wrap--line'>\
            <h2 class='c-slide__title'>\
              The\
              <span class='c-slide__title--medium'>Manipal Marathon 2019</span>\
            </h2>\
          </div>\
        </div>\
      </div>\
      "
    );
  }

  jQuery(".c-slider-init").slick({
    dots: false,
    nav: false,
    arrows: false,
    infinite: true,
    speed: 1500,
    autoplaySpeed: 4000,
    slidesToShow: 1,
    adaptiveHeight: true,
    autoplay: true,
    draggable: false,
    pauseOnFocus: false,
    pauseOnHover: false
  });

  jQuery(".slick-current").addClass("initialAnimation");

  let transitionSetup = {
    target: ".slick-list",
    enterClass: "u-scale-out",
    doTransition: function() {
      var slideContainer = document.querySelector(this.target);
      slideContainer.classList.add(this.enterClass);
      jQuery(".slick-current").removeClass("animateIn");
    },
    exitTransition: function() {
      var slideContainer = document.querySelector(this.target);
      setTimeout(() => {
        slideContainer.classList.remove(this.enterClass);
        jQuery(".slick-current").addClass("animateIn");
      }, 3000);
    }
  };

  var i = 0;
  // On before slide change
  jQuery(".c-slider-init").on("beforeChange", function(
                              event,
                               slick,
                               currentSlide,
                               nextSlide
                              ) {
    if (i == 0) {
      event.preventDefault();
      transitionSetup.doTransition();
      i++;
    } else {
      i = 0;
      transitionSetup.exitTransition();
    }

    jQuery(".c-slider-init").slick("slickNext");
    jQuery(".slick-current").removeClass("initialAnimation");
  });


  
});