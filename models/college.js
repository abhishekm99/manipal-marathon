const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const College = sequelize.define('college', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name:{
        type:Sequelize.STRING,
        allowNull:false
    },
},
{
    timestamps: false,
})

module.exports = College;