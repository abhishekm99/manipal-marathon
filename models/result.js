const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Result = sequelize.define('result', {
    Bib: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    Position:{
        type:Sequelize.STRING,
        allowNull:false
    },
    Name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    Time:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    Distance: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Category: {
        type:Sequelize.STRING,
        allowNull:false,
    },
    Sex: {
        type:Sequelize.STRING,
        allowNull:false,
    },
},
{
    timestamps: false,
})

module.exports = Result;