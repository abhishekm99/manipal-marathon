const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const User = sequelize.define('user', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    username:{
        type:Sequelize.STRING,
        allowNull:false
    },
    email:{
        type:Sequelize.STRING,
        allowNull:false
    },
    password:{
        type:Sequelize.STRING,
        allowNull:true
    },
    rtype:{
        type:Sequelize.STRING,
        allowNull:true  
    },
    isLoggedin: {
        type:Sequelize.BOOLEAN,
        allowNull:false,
    },
    utype:{
        type:Sequelize.STRING,
        default: 'organiser',
        allowNull:false,
    },
    resetToken: Sequelize.STRING,
    resetTokenExpiration: Sequelize.DATE,
    regid:{
        type:Sequelize.STRING,
        default: 'organiser',
        allowNull:true,
    },
},
{
    timestamps: false,
})

module.exports = User;