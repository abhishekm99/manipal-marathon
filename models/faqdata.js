const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Faqdata = sequelize.define('faqdata', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    quest:{
        type:Sequelize.STRING,
        allowNull:false
    },
    answer:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    category: {
        type:Sequelize.STRING,
        allowNull:false,
    },
},
{
    timestamps: false,
})

module.exports = Faqdata;