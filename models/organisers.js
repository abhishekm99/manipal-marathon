const Sequelize = require('sequelize');

const sequelize = require('../helpers/database');

const Organiser = sequelize.define('organiser', {
    id: {
        type:Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    description:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    imageUrl: {
        type:Sequelize.STRING,
        allowNull:false,
    },
},
{
    timestamps: false,
})

module.exports = Organiser;