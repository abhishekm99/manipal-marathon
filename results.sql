-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: marathon
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `results`
--

DROP TABLE IF EXISTS `results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `results` (
  `Bib` int(11) NOT NULL AUTO_INCREMENT,
  `Position` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Time` text,
  `Distance` varchar(255) NOT NULL,
  `Category` varchar(255) NOT NULL,
  `Sex` varchar(255) NOT NULL,
  PRIMARY KEY (`Bib`)
) ENGINE=InnoDB AUTO_INCREMENT=52072 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `results`
--

LOCK TABLES `results` WRITE;
/*!40000 ALTER TABLE `results` DISABLE KEYS */;
INSERT INTO `results` VALUES (1057,'1','Basavaraj Neelappa Godi','0:17:19','5 KM','18 to 40 yrs','M'),(1060,'3','Dhareppa Basavanni Magadum','0:17:41','5 KM','18 to 40 yrs','M'),(1090,'2','Bellanayaka HM','0:17:40','5 KM','18 to 40 yrs','M'),(2001,'1','Shaji N P','0:22:25','5 KM','41 to 55 yrs','M'),(2024,'2','Vinayakumar','0:23:25','5 KM','41 to 55 yrs','M'),(2029,'3','Medappa','0:25:21','5 KM','41 to 55 yrs','M'),(3006,'2','Ramakrishna R','0:29:18','5 KM','56 yrs and above','M'),(3007,'1','Ramayya','0:26:27','5 KM','56 yrs and above','M'),(4026,'3','Mudiyappa','0:21:54','5 KM','MAHE Student','M'),(4247,'2','Aditya Sharma','0:21:24','5 KM','MAHE Student','M'),(4283,'1','Shashank shetty','0:21:03','5 KM','MAHE Student','M'),(5014,'1','Nirmal Krishnan M','0:22:32','5 KM','MAHE Staff 25 to 50 yrs','M'),(5049,'2','Telen Bardoloi','0:22:50','5 KM','MAHE Staff 25 to 50 yrs','M'),(6005,'2','Alapati Vittaleswar','0:46:23','5 KM','MAHE Staff Above 50 yrs','M'),(6008,'1','Kusha N Salian','0:32:44','5 KM','MAHE Staff Above 50 yrs','M'),(7034,'3','Chikkamma M K','0:22:55','5 KM','18 to 40 yrs','F'),(7063,'2','Deeksha B','0:21:16','5 KM','18 to 40 yrs','F'),(7066,'1','Harshita','0:21:15','5 KM','18 to 40 yrs','F'),(8016,'1','Hmmemalatha wagh','0:30:52','5 KM','41 to 55 yrs','F'),(8017,'2','Lathakumari','0:35:32','5 KM','41 to 55 yrs','F'),(8022,'3','Dr Sandhya Aletty','0:39:35','5 KM','41 to 55 yrs','F'),(9008,'1','Meera nayak','0:38:10','5 KM','56 yrs and above','F'),(9009,'2','Sulatha Kamath','0:41:19','5 KM','56 yrs and above','F'),(10148,'1','Nilufer Tamatgar','0:26:10','5 KM','MAHE Student','F'),(10193,'3','Deepthi Prabhu','0:31:47','5 KM','MAHE Student','F'),(11022,'1','Chitralekha D.V','0:32:46','5 KM','MAHE Staff 25 to 50 yrs','F'),(11023,'2','Shalini H','0:33:50','5 KM','MAHE Staff 25 to 50 yrs','F'),(12001,'1','Anitha Lobo','0:33:06','5 KM','MAHE Staff Above 50 yrs','F'),(12002,'2','Dr Mary Mathew','1:10:13','5 KM','MAHE Staff Above 50 yrs','F'),(13024,'2','Sandeepu R','0:34:16','10 KM','18 to 40 yrs','M'),(13025,'3','shreedhara','0:34:25','10 KM','18 to 40 yrs','M'),(13035,'1','Man Chodhary','0:33:02','10 KM','18 to 40 yrs','M'),(14006,'1','Joison Prashanth Abrev','0:42:13','10 KM','41 to 55 yrs','M'),(14015,'2','Subhas','0:47:37','10 KM','41 to 55 yrs','M'),(15004,'1','Hosur Udayakumar Shetty','0:49:47','10 KM','56 yrs and above','M'),(15005,'2','Madhava saripalla','0:52:21','10 KM','56 yrs and above','M'),(16047,'2','Mohith H','41:12:00','10 KM','MAHE Student','M'),(17001,'2','Nikhil Aithal','55:26:00','10 KM','MAHE Staff 25 to 50 yrs','M'),(17037,'1','Lumchio Odiuo','46:12:00','10 KM','MAHE Staff 25 to 50 yrs','M'),(18004,'1','Suresh Pillai','59:31:00','10 KM','MAHE Staff Above 50 yrs','M'),(18005,'2','Udaya G Poojary','1:08:51','10 KM','MAHE Staff Above 50 yrs','M'),(19007,'1','Chaitra Devadiga','0:40:39','10 KM','18 to 40 yrs','F'),(19009,'2','Priya LD','0:40:41','10 KM','18 to 40 yrs','F'),(19027,'3','Tippavva','0:42:03','10 KM','18 to 40 yrs','F'),(20002,'3','Kalpana Bhat','1:19:11','10 KM','41 to 55 yrs','F'),(20004,'2','Susheela Konanhalli','1:13:20','10 KM','41 to 55 yrs','F'),(20005,'1','Manjula B L','1:00:51','10 KM','41 to 55 yrs','F'),(21003,'2','Lalitha naik','1:26:18','10 KM','56 yrs and above','F'),(21004,'1','Arunakala Rao','1:17:43','10 KM','56 yrs and above','F'),(22004,'2','Zeal Kadakia','1:03:35','10 KM','MAHE Student','F'),(22042,'1','Riya Pradhan','48:58:00','10 KM','MAHE Student','F'),(22049,'1','Sangameesh H','37:14:00','10 KM','MAHE Student','M'),(23001,'2','Sanjukta','1:09:03','10 KM','MAHE Staff 25 to 50 yrs','F'),(24001,'2','Anitha Raghunathan','1:23:49','10 KM','MAHE Staff Above 50 yrs','F'),(24002,'1','Vidya Pratap','1:16:11','10 KM','MAHE Staff Above 50 yrs','F'),(25018,'3','Niccodamus Kiprugut','1:15:01','21 KM','18 to 40 yrs','M'),(26010,'3','Shyam Poojary','1:47:58','21 KM','41 to 55 yrs','M'),(26016,'2','Deepak khatawkar','1:47:19','21 KM','41 to 55 yrs','M'),(26024,'1','Chandrashekara','1:28:48','21 KM','41 to 55 yrs','M'),(27006,'2','Rajeeva shetty','1:42:27','21 KM','56 yrs and above','M'),(28015,'1','Shashank TR','1:47:35','21 KM','MAHE Student','M'),(28016,'2','Sam Gabriel Jerard','1:55:19','21 KM','MAHE Student','M'),(29015,'2','Bulan Barodoli','1:44:51','21 KM','MAHE Staff 25 to 50 yrs','M'),(29016,'1','Paresh Barodoli','1:41:10','21 KM','MAHE Staff 25 to 50 yrs','M'),(30002,'2','Prashanth','2:44:33','21 KM','MAHE Staff Above 50 yrs','M'),(30003,'1','Arun Shanbhag','2:15:09','21 KM','MAHE Staff Above 50 yrs','M'),(31001,'3','Shalini K S','1:44:31','21 KM','18 to 40 yrs','F'),(31006,'1','Archana','1:27:02','21 KM','18 to 40 yrs','F'),(32001,'1','Roli Awasthi','2:54:53','21 KM','41 to 55 yrs','F'),(32002,'2','Tapati Bhattacharya','3:02:01','21 KM','41 to 55 yrs','F'),(34002,'1','Sharvari Karnik','2:17:25','21 KM','MAHE Student','F'),(34007,'2','Saumya Bhagat','2:25:25','21 KM','MAHE Student','F'),(35001,'2','Dr.Prameela ','2:33:59','21 KM','MAHE Staff 25 to 50 yrs','F'),(35002,'1','Dr.Sahana','2:19:20','21 KM','MAHE Staff 25 to 50 yrs','F'),(36001,'1','Dr.Sudha Menon','2:37:36','21 KM','MAHE Staff Above 50 yrs','F'),(37010,'2','Japhet Ronno','2:33:40','42 KM','18 to 40 yrs','M'),(38003,'2','Satish Chandra','4:10:04','42 KM','41 to 55 yrs','M'),(38010,'3','George Varghese','4:14:36','42 KM','41 to 55 yrs','M'),(38013,'1','Vishwanath Kotian','3:48:17','42 KM','41 to 55 yrs','M'),(39001,'2','Vittala Shettigara','4:30:13','42 KM','56 yrs and above','M'),(39002,'1','Viswanath Shetye','3:59:17','42 KM','56 yrs and above','M'),(40003,'1','Anish Aithal','3:32:27','42 KM','MAHE Student','M'),(40011,'2','Ashank','3:38:23','42 KM','MAHE Student','M'),(41001,'1','Sudeep kumar','3:10:03','42 KM','MAHE Staff 25 to 50 yrs','M'),(41002,'2','Gangadhar L','4:30:10','42 KM','MAHE Staff 25 to 50 yrs','M'),(42000,'1','Dr.Girish Menon ','5:18:40','42 KM','MAHE Staff Above 50 yrs','M'),(43000,'3','Priyanka H B','4:43:07','42 KM','18 to 40 yrs','F'),(43001,'2','Deepika Prakash','3:55:39','42 KM','18 to 40 yrs','F'),(49004,'1','Bhoomika','3:37:09','42 KM','18 to 40 yrs','F'),(49008,'1','Stephen','2:33:38','42 KM','18 to 40 yrs','M'),(49010,'3','Eliud Kimaru Mutai','2:35:40','42 KM','18 to 40 yrs','M'),(50005,'1','Dinesh','1:13:47','21 KM','18 to 40 yrs','M'),(50006,'2','Pravin khambal','1:14:05','21 KM','18 to 40 yrs','M'),(50014,'1','Thomas PC','1:36:48','21 KM','56 yrs and above','M'),(50015,'2','Sandhya kj','1:33:13','21 KM','18 to 40 yrs','F'),(51077,'1','Gouthami','1:04:23','10 KM','MAHE Staff 25 to 50 yrs','F'),(51081,'3','bhaganna','0:50:40','10 KM','41 to 55 yrs','M'),(52071,'2','Tye jia ying','0:29:21','5 KM','MAHE Student','F');
/*!40000 ALTER TABLE `results` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-11 14:47:37
