-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: marathon
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `organisers`
--

DROP TABLE IF EXISTS `organisers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `imageUrl` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisers`
--

LOCK TABLES `organisers` WRITE;
/*!40000 ALTER TABLE `organisers` DISABLE KEYS */;
INSERT INTO `organisers` VALUES (1,'Dr.Girish Menon','All of MRC looks up to Girish Sir as a beacon of inspiration and dedication. Being a doctor in itself is a Herculean task, but being an active runner and running over 150 km every month is something next to impossible. One of the senior most runners in the club, he makes every runner’s excuses seem small. ','../images/og/Girish sir.jpeg'),(2,'Dr. Shobha Erappa','Radiant and ever smiling are some of the words every student would use to describe Shobha ma’am. She handles all official and university work for Manipal Runners and makes organising events much easier. She is also a passionate runner and races her heart out!','../images/og/Shobha Ma\'am.jpeg'),(3,'Navaal Rai','A passionate ultramarathoner, he perfectly fits the role of the President of the Club. As a sponsored athlete he has great knowledge about various sporting and nutrition products! Even though he’s one of the fastest runners in Manipal, he’ll always slow down if it means helping a beginner finish their run. He\'s currently preparing to run his first 110km race!','../images/og/Navaal.jpg'),(4,'Likhith Shivaprasad','The Vice President of the Club, Likhith is always looking for opportunities to push himself through crazy challenges. Strike up a casual conversation with him and you’ll find out how incredibly knowledgeable he is about the sport. As of now he is probably preparing for his next wacky challenge!','../images/og/Likhith.jpg'),(5,'Anish Aithal','Hardworking and happy-go-lucky are some of the words that come to mind while describing Anish. The smile on his face while he runs at super fast paces says more than words can describe; about his passion for running. As a 12hr Stadium runner, he has one of the strongest minds in MRC!','../images/og/Anish.jpg'),(6,'Ishan Trivedi','Inspite of battling various injuries Ishan has come out as one of the strongest runners in Manipal. With his knowledge from the NDA, he keeps enlightening us about how to stay fit and disciplined! Just as with his running, he has a high level of commitment to whatever work comes his way','../images/og/Ishan.jpg'),(7,'Sriya Peri','MRC is not just its runners, its every single person who helps put together the various events. Working hard in a runners club and giving people joy while not being a runner herself, she comes up with some of the best and most creative ideas MRC has ever seen. ','../images/og/Sriya.jpg'),(8,'Yashas Chakole','A runner is strong; but a runner with flatfeet is a hero! He is, without a doubt, one of the most hardworking and strong runner in the club. Inspite of his training and academics he manages to create all of the graphics and artwork for MRC and the Marathon. He\'s currently prepping for a gruelling 50k race held in Pune!','../images/og/Yashas.jpeg');
/*!40000 ALTER TABLE `organisers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-09 19:14:39
