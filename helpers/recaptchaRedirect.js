const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '/../.env') });
const captchatest = require('../config/gcaptcha-test');

const request = require('request');

const secrets = {
  v2: captchatest.testing.v2,
  v3: captchatest.testing.v3,
  invisible: captchatest.testing.invisible,
}

module.exports = async (req,res,next) => {
  if(process.env.MODE == 'DEV' || req.body["g-recaptcha-response"] === process.env.MOBILE_APP_REGLOG_KEY) return next();
  console.log(req.body)
  let data = {
    secret: secrets['invisible'],
    response: req.body["g-recaptcha-response"]
  };
  let options = {
    method: 'post',
    form: data,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    url: 'https://www.google.com/recaptcha/api/siteverify'
  };
  request.post(options, function (err, HTTPResponse, body) {
    if (err) {
        console.log(err)
        req.flash('errors','Captcha Invalidated')
        return res.redirect('/register');
    }
    body = JSON.parse(body);
    if (body.success == true) {
      console.log('success')
      return next();
    }
    else {
      req.flash('errors','Captcha Invalidated')
      return res.redirect('/register');
    }
  });
}
