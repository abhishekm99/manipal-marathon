exports.getAboutPage = (req,res,next) => {
    res.render('about', {
        pageTitle: 'About Us',
        notMain: true,
    })
}

exports.getPrizes = (req,res,next) => {
    res.render('prizes', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getPrizes5k = (req,res,next) => {
    res.render('prizesfive', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getPrizes10k = (req,res,next) => {
    res.render('prizesten', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getPrizes21k = (req,res,next) => {
    res.render('prizestwo', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getPrizes42k = (req,res,next) => {
    res.render('prizesfour', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getPrizesmahestud = (req,res,next) => {
    res.render('prizesmahestud', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getPrizesmahefac = (req,res,next) => {
    res.render('prizesmahefac', {
        pageTitle: 'Prizes',
        notMain: true, 
    })
}

exports.getCertificates = (req,res,next) => {
    res.render('certs', {
        pageTitle: 'Certificates',
        notMain: true,
    })
}