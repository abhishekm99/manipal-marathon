const Member = require('../models/registeredmem');
const User = require('../models/users');
const College = require('../models/college');
const Paytm = require('../models/paytm');
const uuidv4 = require('uuid/v4');
const credentials = require('../config/paytm-creds');
const meta = require('../config/paytm-meta');
const cs = require('../helpers/checksum');
const axios = require('axios');
const nodemailer = require('nodemailer');
const mailGun = require('nodemailer-mailgun-transport');
const { check, validationResult } = require('express-validator');
var Tesseract = require('tesseract.js')
var request = require('request')
var fs = require('fs')

exports.getRegisterPage = (req,res,next) => {
    College.findAll().then(result => {
        res.render('register', {
            pageTitle: 'Register',
            notMain: true,
            colleges: result,
            success: req.session.success,
            errors: req.flash('errors'),
            formData: req.flash('form')[0],
        })
    })
}

exports.postRegister = (req,res,next) => {
    var errors = validationResult(req).array();
    var formData= {
        name: req.body.name,
        email:req.body.email,
        contact: req.body.contact,
        age: req.body.age,
        maheval: req.body.maheval,
        emgname: req.body.emgname,
        emgcontact: req.body.emgcontact
    }
    if (errors) {
        req.flash('errors',errors)
        req.flash('form', formData)
        req.session.success = false;
        return res.redirect('/register');
    }
    const name = req.body.name;
    const email = req.body.email;
    const contact = req.body.contact;
    const age = req.body.age;
    const gender = req.body.gender;
    const maheval = req.body.maheval;
    var institute;
    let oid;
    const regno = req.body.regno;
    const ticket = req.body.rtype;
    const emgname = req.body.emgname;
    const emgcontact = req.body.emgcontact;
    const tshirt = req.body.tshirt;
    const createdAt = new Date();
    let amount;
    oid = uuidv4();
    if(req.body.institute[0] !== 0){
        institute = req.body.institute[0];
    }
    else{
        institute = req.body.institute[1];
    }
    if(maheval == 1){
        amount = 10;
    }
    else{
        amount = 20;
    }
    
    
    Member.findOne({where:{Attendee_Email:email}}).then(userDoc => {
        if(userDoc){
            req.flash('error', 'Email already registered, please contact administrator');
            return res.redirect(`/register`);
        }

        Member.create({
            BiB_no: '123456',
            Attendee_Name: name,
            Attendee_Email:email,
            Ticket_Name: ticket,
            Contact_Number: contact,
            Gender:gender,
            Age: age,
            MAHE: maheval,
            Institution_Name:institute,
            Employee_Number:regno,
            Emergency_Contact_Name: emgname,
            Emergency_Contact_Number:emgcontact,
            Order_Id:oid,
            T_Shirt_Size: tshirt,
            createdAt: createdAt,
            approved: 0,
            paymentStatus: 0,
        }).then(result => {
            user = result.dataValues;
            User.create({
                username: user.Attendee_Name,
                email: user.Attendee_Email,
                password: 'Not set',
                rtype: 'member',
                isLoggedin: 0,
                utype: 'member',
                regid: user.Registration_Id,
            }).then(result => {
                return res.sendSuccess(null, 'User Created')
            }).catch(err => {throw new Error(err)})
            if(result.dataValues.Ticket_Name === '3k'){
                return res.redirect('/register');
            }
            else{
                Paytm.create({
                    Order_Id: oid,
                    Cust_Id: result.dataValues.Registration_Id,
                    Amount: amount,
                    Contact: contact,
                    Email: email
                }).then(result => {
                    res.redirect(`/payment?orderid=${oid}&callback=http://localhost:3000/paytmCallback`)
                })
            }
            
            // const datasent = {
            //     name: name,
            //     oid: oid,
            //     email: email,
            //     contact: contact,
            //     custid: result.dataValues.Registration_Id,
            //     amount: amount
            // }
        }).catch(err => console.log(err))
})


}

exports.getPayments = (req,res,next) => {
    let oid = req.query.oid;
    let callback = req.query.callback;
    Paytm.findOne({where:{Order_Id:oid}}).then(result => {
        Member.findOne({where:{Registration_Id:result.dataValues.Cust_Id}}).then(result => {
            return res.render('payments', {
                title:'Payment',
                oid:oid,
                data:result,
                notMain:true,
            })
        })
    })
}

exports.makePayments = (req,res,next) => {
    let err, result, params, url, key;
    if(req.query.retry){
        Member.findOne({where:{}})
    }
    Paytm.findOne({where:{Order_Id:req.query.orderid}}).then(result => {
        let txn = result;
        params = credentials.staging;
        url = meta.staging.txn_url;
        key = meta.staging.key;
        params["ORDER_ID"] = txn.dataValues.Order_Id;
        params["CUST_ID"] = txn.dataValues.Cust_Id;
        params["TXN_AMOUNT"] = txn.dataValues.Amount;
        params["CALLBACK_URL"] = req.query.callback;
        params["MOBILE_NO"] = txn.dataValues.Contact;
        params["EMAIL"] = txn.dataValues.Email;
        console.log(params);
        cs.genchecksum(params,key,(err, params)=>{
            return res.render('redirect', { url, params });
        });
    })
    .catch(err => {console.log(err)});
    // [err, result] = await to(conn.query("SELECT * FROM paytm WHERE order_id = ? AND status ='OPEN'",[req.query.orderid]));
    // if(err || result.length === 0) {
    //   console.log(err);
    //   return res.render('failure');
    // }
    
}


exports.getPaytmCallback = (req,res,next) => {
    return res.render('paytmCallback', {
        title: 'Transaction Status',
        notMain: true,
    })
}

exports.postPaytmCallback = (req,res,next) => {
    return res.redirect('/paytmCallback?oid=' + req.body.ORDERID);
}

exports.getTransactionStatus = (req,res,next) => {
    let err, result, txn, response, url, key;
    console.log('reached txn status stage');
    Paytm.findOne({where:{Order_Id:req.body.orderid}}).then(result => {
        txn = result.dataValues;
        let query_params = { ORDERID: txn.Order_Id };
        if(txn.status == 'TXN_SUCCESS' || txn.status == 'TXN_FAILURE' || txn.status == 'INVALID') {
            txn.success = true;
            return res.json({ txn });
        }
        // if(txn.type == 'TESTING') {
            query_params.MID = credentials.staging.MID;
            url = meta.staging.status_url;
            key = meta.staging.key;
        /* } else {
            query_params.MID = credentials.production.MID;
            url = meta.production.status_url;
            key = meta.production.key;
        } */
        cs.genchecksum(query_params,key, async (err, query_params) => {
            // [err, response] = await to(axios.post(url,query_params));
            // axios.defaults.headers.post['X-CSRF-Token'] = res.data._csrf;
            // axios.defaults.headers.common['x-csrf-token'] = readCookie('XSRF-TOKEN');
            axios.post(url,query_params).then(response => {
                response = response.data;
                console.log(response);
                txn.paytmid = response.TXNID;
                txn.banktxnid = response.BANKTXNID;
                txn.amount_paid = response.TXNAMOUNT;
                txn.txndate = response.TXNDATE;
                txn.status = response.STATUS;
                if(response.STATUS == 'TXN_SUCCESS') {
                    if(parseInt(txn.Amount) === parseInt(response.TXNAMOUNT)) {
                        /* if(txn.type == 'SPORTS') {
                            axios.get(
                                'https://sports.mitrevels.in/api/emailstatus/'+req.body.orderid+'?token=arandomafauthtoken'
                            ).then().catch(console.log);
                        } else if (txn.type == 'DASHBOARD' || txn.type == 'TESTING') { */
                            axios.post(
                                'http://localhost:3000/updatePaymentStatus',
                                {
                                order_id: req.body.orderid
                                }
                            ).then().catch(console.log);
                        // }
                        //-----------------------------------------------------
                        //Make nodemailer connect here to email reciept
                        //-------------------------------------------------------
                        const auth = {
                            auth: {
                                api_key: process.env.API_KEY || 'key-0bb9d116b294ed0e0a61a379d4a3aeb1', // TODO: 
                                domain: process.env.DOMAIN || 'mailer.manipalmarathon.in' // TODO:
                            }
                        };

                        let transporter = nodemailer.createTransport( mailGun(auth) );
                                transporter.sendMail({
                                        from: 'noreply@manipalmarathon.in', // TODO: email sender
                                        to: `${txn.Email}`, // TODO: email receiver
                                        subject: 'Payment reciept for Manipal Marathon 2021',
                                        html: `
                                                <p>Dear Participant,</p>
                                                <p style='font-weight:bold;'>Please find attached your recipet for Manipal Marathon 2021</p>
                                                <table>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>${txn.Email}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Order Id</th>
                                                        <td>${txn.Order_Id}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Amount Paid</th>
                                                        <td>${txn.amount_paid}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Paytm Id</th>
                                                        <td>${txn.paytmid}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Bank Id</th>
                                                        <td>${txn.banktxnid}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Date of Transaction</th>
                                                        <td>${txn.txndate}</td>
                                                    </tr>
                                                </table>
                                        `
                                    }, (err, data) => {
                                        if (err) {
                                            console.log(err)
                                            return log('Error occurs for', txn.Cust_Id);
                                        }
                                        return log('Email sent for ', txn.Cust_Id);
                                });
                
                            transporter.close();
                        /* await to(axios.post(meta.mailer.url,{
                        email: txn.email,
                        orderid: txn.order_id,
                        amount: txn.amount_paid,
                        paytmid: txn.paytmid,
                        type: txn.type,
                        auth: meta.mailer.key
                        })); */
                    }
                    else txn.status = 'INVALID';
                } else txn.amount_paid = 0.0;
                
                txn.updated_at = undefined;
                delete txn.updated_at;
                Paytm.findOne({where:{Order_Id:txn.Order_Id}})
                    .then(data => {
                        console.log(data);
                        data.created_at = new Date;
                        data.updated_at = txn.updated_at;
                        data.paytmid = txn.paytmid;
                        data.banktxnid = txn.banktxnid;
                        data.status = txn.status;
                        data.amount_paid = txn.amount_paid;
                        data.txndate = txn.txndate;
                        return data.save();
                    }).catch(err => console.log(err))
                // [err, result] = await to(conn.query('UPDATE paytm SET ? WHERE order_id = ?',[txn,txn.order_id]));
                txn.success = true;
                return res.json(txn);
            }).catch(err => console.log(err));
            if(err) return res.json({ success: false });
        });
    }).catch(err => console.log(err));
  /* [err, result] = await to(conn.query('SELECT * FROM paytm WHERE order_id = ?',[req.body.orderid]));
  if(err || !result || result.length == 0) return res.json({ success: false });
  txn = result[0]; */
  
}

exports.updatePaymentStatus = (req,res,next) => {
    let order_id = req.body.order_id , result;
    Member.findOne({where:{Order_Id: req.body.order_id}}).then(data => {
        data.paymentStatus = 1;
        return data.save();
    }).then(result => {
        console.log(result);
        return res.sendSuccess(null, 'Updated details');
    })
    // try {
    //     result = await db.query("UPDATE card_bought SET paid='1' WHERE order_id=?", [order_id]);
    //     if (result.changedRows == 0){
    //     result = await db.query("UPDATE play_card_bought SET paid='1' WHERE order_id=?", [order_id]);
    //     if( result.changedRows > 0 ){
    //         result = await db.query("SELECT * FROM play_card_bought WHERE order_id=?", [order_id]);
    //         if( result.length <= 0 ) return res.sendError(null);
    //         let user = result[0];
    //         result = await db.query("UPDATE informals_users SET no_of_tokens = no_of_tokens + 5 WHERE userid = ?",[ user.delid ]);
    //     }
    //     return res.sendSuccess(null, 'Updated Details');
    //     }
    //     result = await db.query("SELECT * FROM card_bought WHERE order_id = ?", [order_id]);
    //     result = result[0];
    //     // proshow
    //     // console.log(result);
    //     /*if (result.card_type == '8') {
    //     // console.log('in here');
    //     await proshow_add_ticket(result.delid);
    //     }*/
    //     return res.sendSuccess(null, 'Updated details');
    // } catch (err) {
    //     return res.sendError(err);
    // }
}

exports.getTesserect = (req,res,next) => {
var url = 'http://tesseract.projectnaptha.com/img/eng_bw.png'
var filename = 'pic.png'

var writeFile = fs.createWriteStream(filename)

request(url).pipe(writeFile).on('close', function() {
  console.log(url, 'saved to', filename)
  Tesseract.recognize(filename)
    .progress(function  (p) { console.log('progress', p)  })
    .then(function (result) {
      console.log(result.text)
      process.exit(0)
    })
    .catch(err => console.error(err))
});
}