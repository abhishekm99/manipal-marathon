const Organiser = require('../models/organisers');
exports.getOrganisers = (req,res,next) => {
    Organiser.findAll({order: [
        ['id', 'ASC'],
    ]}).then(result => {
        res.render('organiser', {
            title: 'Organisers',
            organisers: result,
            notMain: true,
        })
    })
}