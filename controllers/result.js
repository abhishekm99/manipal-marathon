const Result = require('../models/result');
const { Op } = require("sequelize");

exports.getMain = (req,res,next) => {
    res.render('results/mainhandle', {
        pageTitle: 'Results',
        notMain: true,
    })
}

exports.getFinal = (req,res,next) => {
    console.log(req.params.gender, req.params.cat, req.params.rtype)
    Result.findAll({
        where: {
          Distance: {
            [Op.like]: req.params.rtype
          },
          Sex: {
            [Op.like]: req.params.gender
          },
          Category: {
            [Op.like]: req.params.cat
          },
        },
        order: [
          ['Position', 'ASC'],
        ],
      }).then(result => {
          console.log(result);
        res.render('results/finalresult', {
            pageTitle: 'Winners',
            notMain: true,
            winners: result,
            winnerscount: result.length,
        })
    })
    
}